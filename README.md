# pruebTecnicaPeriferiaFront

## Para empezar

- El Front esta construido en angular 16
- Descargue el repositorio

- Proyecto Angular (Front):
  1. Ejecute el comando npm i
  2. Ejecute el comando ng s
  3. En el navegador de su preferencia abra el link http://localhost:4200/

# Composición del programa

1. La aplicación tiene 3 tabs en la parte superior: Registro de Inventarios, Entrega de Inventarios y Crear Producto.
2. En el momento que ingresa en la aplicación si no hay productos en la base de datos relacional crea 3 productos (Tarjeta de Crédito, Cheque y Talonario)
3. En el tab de Crear Producto es posible crear un nuevo producto ingresando el nombre del nuevo producto. Si ya existe ese nombre en la base de datos relacional muestra un mensaje de error donde indica que ya se encuentra el producto creado.
4. En el tab Registro de Inventarios debe llenar todos los datos y al oprimir el boton Guardar y enviar el almacena el registro en la base de datos no relacional
5. En el tab de entrega de inventario podra ver todos los inventarión registrados.
6. Se puede observar que cada inventario registrado tiene un boton que dice si el inventario fue entregado o si se puede entregar. Si uno hace click sobre el boton de entregar cambiara inmediatamente el estado a entregar y si hace click sobre el boton del inventario que ya fue entregado aparece un mensaje donde se menciona que el inventario ya fue entregado.