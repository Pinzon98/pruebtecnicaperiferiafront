export class InventarioDTO {
    usuario: string;
    producto: string;
    numeroSerie: number;
    fecha: string;

    constructor(usuario: string, producto: string, numeroSerie: number, fecha: string){
        this.usuario = usuario;
        this.producto = producto;
        this.numeroSerie = numeroSerie;
        this.fecha = fecha;
    }
}
