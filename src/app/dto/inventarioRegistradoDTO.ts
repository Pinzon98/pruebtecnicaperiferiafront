export class InventarioRegistradoDTO {
    id: string;
    producto: string;
    numeroSerie: number;
    estado: string;
    fechaModificacion: string;

    constructor(id: string, producto: string, numeroSerie: number, estado: string, fechaModificacion: string){
        this.id = id;
        this.producto = producto;
        this.numeroSerie = numeroSerie;
        this.estado = estado;
        this.fechaModificacion = fechaModificacion;
    }
}
