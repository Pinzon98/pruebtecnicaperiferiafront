import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  imports: [CommonModule],
  standalone: true,
})
export class ModalComponent {
  @Input() showModal: boolean = false;
  @Input() message: string = '';
  @Output() hideModal = new EventEmitter<void>();

  constructor() { }

  close() {
    this.hideModal.emit();
  }
}
