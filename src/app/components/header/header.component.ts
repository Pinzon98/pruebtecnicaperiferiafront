import { Component } from '@angular/core';
import { TabService } from 'src/app/services/tab.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  standalone: true,
})
export class HeaderComponent {
  selectedTab: string = 'tab1';
  
  constructor(private tabService: TabService) { }

  selectTab(tabName: string) {
    this.tabService.updateTab(tabName);
    this.tabService.tabChanged.subscribe(tabName => {
      this.selectedTab = tabName;
    });
  }
}
