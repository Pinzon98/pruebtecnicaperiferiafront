import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { InventarioDTO } from 'src/app/dto/inventarioDTO';
import { InventarioRegistradoDTO } from 'src/app/dto/inventarioRegistradoDTO';
import { ProductoDTO } from 'src/app/dto/productoDTO';
import { ControlProductosService } from 'src/app/services/control-productos.service';
import { TabService } from 'src/app/services/tab.service';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.scss'],
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, CommonModule, NgxSpinnerModule, ModalComponent]
})
export class InventarioComponent implements OnInit{
  formularioRegistoInv!: FormGroup;
  formularioProducto!: FormGroup;
  selectedTab: string = 'tab1';
  tituloTab: string = 'Registro de Inventario';
  showModal: boolean = false;
  modalType: string = '';
  mensajeError: string = '';
  inventariosRegistrados: Array<InventarioRegistradoDTO> = []
  productos: Array<ProductoDTO> = []

  constructor(private fb: FormBuilder, private tabService: TabService, private controlProductoService: ControlProductosService, private spinner: NgxSpinnerService) {}

  ngOnInit() {
    this.generarDatos();
    this.iniciarFormularioRegistroInv();
    this.iniciarFormularioProducto();
    this.getProductos();
    this.tabService.tabChanged.subscribe(tabName => {
      this.cambiarValor(tabName)
      this.selectedTab = tabName;
    });
  }

  iniciarFormularioRegistroInv(){
    this.formularioRegistoInv = this.fb.group({
      usuario: [null, Validators.required],
      producto: [null, Validators.required],
      numeroSerie: [null, Validators.required],
      fecha: [null, Validators.required]
    });
  }

  iniciarFormularioProducto(){
    this.formularioProducto = this.fb.group({
      producto: [null, Validators.required]
    });
  }

  cambiarValor(tabName: string){
    this.iniciarFormularioRegistroInv();
    this.iniciarFormularioProducto();
    if(tabName=='tab1'){
      this.getProductos();
      this.tituloTab = 'Registro de Inventario';
    }else if(tabName=='tab2'){
      this.getInventarios();
      this.tituloTab = 'Entregar Inventario';
    }
    else if (tabName=='tab3'){
      this.tituloTab = 'Crear Producto'
    }
  }

  cambiarEstado(estado: string, id: string){
    this.spinner.show();
    this.controlProductoService.entregarInventarios(id).subscribe((response) => {
      this.spinner.hide();
      if(response.exitoso){
        this.inventariosRegistrados = response.inventarios;
        this.iniciarFormularioRegistroInv();
        this.mostrarModal('success');
      }
      else{
        this.mostrarModal('error');
        this.mensajeError = response.error;
      }
    })
  }

  mostrarModal(type: string) {
    this.modalType = type;
    this.showModal = true;
  }

  hideModal() {
    this.showModal = false;
  }

  guardarInventarios(){
    this.spinner.show();
    const inventario = new InventarioDTO(this.formularioRegistoInv.value.usuario, this.formularioRegistoInv.value.producto, this.formularioRegistoInv.value.numeroSerie, this.formularioRegistoInv.value.fecha);
    this.controlProductoService.guardarInventarios(inventario).subscribe((response) => {
      this.spinner.hide();
      if(response.exitoso){
        this.iniciarFormularioRegistroInv();
        this.mostrarModal('success');
      }
      else{
        this.mostrarModal('error');
        this.mensajeError = response.error;
      }
    });
  }

  generarDatos(){
    this.controlProductoService.generarDatos().subscribe(response => {

    })
  }

  entregarInventarios(id: string){
    this.spinner.show();
    this.controlProductoService.entregarInventarios(id).subscribe(response => {
      this.inventariosRegistrados = response.inventarios;
      this.spinner.hide();
    })
  }

  getProductos(){
    this.spinner.show();
    this.controlProductoService.getProductos().subscribe(response => {
      this.productos = response.productos;
      this.spinner.hide();
    })
  }

  getInventarios(){
    this.spinner.show();
    this.controlProductoService.getInventarios().subscribe(response => {
      this.inventariosRegistrados = response.inventarios;
      this.spinner.hide();
    })
  }

  guardarProducto(){
    this.spinner.show();
    this.controlProductoService.guardarProductos(this.formularioProducto.value).subscribe((response) => {
      this.spinner.hide();
      if(response.exitoso){
        this.iniciarFormularioProducto();
        this.mostrarModal('success');
      }
      else{
        this.mostrarModal('error');
        this.mensajeError = response.error;
      }
    })
  };
}
