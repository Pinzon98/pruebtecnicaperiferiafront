import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TabService {
  selectedTab: string = 'tab1';
  tabChanged: EventEmitter<string> = new EventEmitter();

  constructor() { }

  updateTab(tabName: string) {
    this.selectedTab = tabName;
    this.tabChanged.emit(tabName);
  }
}