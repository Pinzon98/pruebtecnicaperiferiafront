import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ControlProductosService {
  urlBack = 'http://127.0.0.1:8000/'

  constructor(private http: HttpClient) { }

  generarDatos(): Observable<any> {
    return this.http.get<any>(this.urlBack + 'api/v1/');
  }

  guardarInventarios(datos: any): Observable<any> {
    return this.http.post<any>(this.urlBack + 'api/v1/registroInventario', datos);
  }

  entregarInventarios(id: string): Observable<any> {
    return this.http.get<any>(this.urlBack + 'api/v1/entregaInventario/' + id);
  }

  getProductos(): Observable<any> {
    return this.http.get<any>(this.urlBack + 'api/v1/obtenerProductos');
  }

  getInventarios(): Observable<any> {
    return this.http.get<any>(this.urlBack + 'api/v1/obtenerInventarios');
  }

  guardarProductos(datos: any): Observable<any>{
    return this.http.post<any>(this.urlBack + 'api/v1/registroProducto', datos);
  }
}
